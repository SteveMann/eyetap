# EyeTap

EyeTap.org website

## Description
The official of website of the father of wearable tech (Steve Mann).

## Contributing
We welcome any contributions, that is why we are here on GitLab!

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Perry Toone - https://ptoone.com
WaterHCI - https://waterhci.com

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
